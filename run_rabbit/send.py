import pika

credentials = pika.PlainCredentials('admin', 'admin')
# while running from docker exec use rabbit else localhost
connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbit', credentials=credentials))
channel = connection.channel()


channel.queue_declare(queue='hello')

channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='ajajaja')
print(" [x] Sent 'Hello World!'")
connection.close()